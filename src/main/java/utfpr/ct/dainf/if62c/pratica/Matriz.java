package utfpr.ct.dainf.if62c.pratica;

/**
 * Representa uma matriz de valores {@code double}.
 * @author Wilson Horstmeyer Bogadao <wilson@utfpr.edu.br>
 */
public class Matriz {
    
    // a matriz representada por esta classe
    private final double[][] mat;
    
    /**
     * Construtor que aloca a matriz.
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     */
    public Matriz(int m, int n) {
        mat = new double[m][n];
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     */
    public Matriz getTransposta() {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param c A matriz a ser somada
     * @return A soma das matrizes
     */
    public Matriz soma(Matriz c) {
        
        int i, j;
        Matriz s = new Matriz (3,2);
 
        for(i = 0; i<3;i++)
        {
            for(j=0; j<2;j++)
            {
               s.mat[i][j] = c.mat[i][j] + mat [i][j];
            }
        }
        return s;
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @param n
     * @return O produto das matrizes
     */
    public Matriz prod(Matriz n) {
        int i, j, k;
        int soma =0;
        Matriz r = new Matriz (3,3);
       
        for(i = 0; i<3;i++)
        {
            for(j=0; j < 3;j++)
            {
                for(k=0;k < 2; k++)
                {
                r.mat[i][j] += mat [i][k] * n.mat[k][j];
                }
                
            }  
        }
        return r;
    }
    

    /**
     * Retorna uma representação textual da matriz.
     * Este método não deve ser usado com matrizes muito grandes
     * pois não gerencia adequadamente o tamanho do string e
     * poderia provocar um uso excessivo de recursos.
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha: mat) {
            s.append("[ ");
            for (double x: linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }

}