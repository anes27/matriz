import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        Matriz secsoma = new Matriz(3, 2);
        double [][]c = secsoma.getMatriz();


        m[0][0] = 0.0;
        m[0][1] = 1.0;
        m[1][0] = 2.0;
        m[1][1] = 3.0;
        m[2][0] = 4.0;
        m[2][1] = 5.0;
        
        
        c[0][0] = 1.0;
        c[0][1] = 2.0;
        c[1][0] = 3.0;
        c[1][1] = 4.0;
        c[2][0] = 5.0;
        c[2][1] = 6.0;
        

        Matriz transp = orig.getTransposta();
        Matriz s = orig.soma(secsoma);
        
        
        Matriz mult = orig.prod(transp);
        
        
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        
        System.out.println("Matriz soma: " + s);
        
        System.out.println("Matriz produto: " + mult);
    }
}
 